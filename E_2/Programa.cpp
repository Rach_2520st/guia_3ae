#include <iostream>
using namespace std;

#include "Lista.h"

void imprimir(Nodo *tmp){
	while(tmp!= NULL){
		cout << tmp->nro << endl;
		tmp = tmp->sgte;
	}
	cout << endl << endl;
}
Nodo* ordenar_lista(Nodo* p){
	Nodo* segundo = p;
	Nodo* actual = NULL;
	int siguiente;
	while (p != NULL){
		actual = p->sgte;
		while(actual != NULL){
			if(p->nro > actual->nro ){
				siguiente = p->nro;
				p->nro = actual->nro;
				actual->nro = siguiente;
			}
			actual = actual->sgte;
		}
		p = p->sgte;
	}
	return segundo;

}
Nodo* mezcla(Nodo* primero, Nodo* segundo){
	Nodo *final = NULL;
	int siguiente;
	Lista *lista = new Lista();

	while(primero != NULL){
		final = lista->crear(primero->nro);
		primero = primero->sgte;
	}
	while(segundo != NULL){
		final = lista->crear(segundo->nro);
		segundo = segundo->sgte;
	}
	imprimir(final);	

}
int main(void)
{
	Lista *lista1 = new Lista();
	Lista *lista2 = new Lista();
	int nro;

	Nodo *pUlt = NULL;
	Nodo *primero = NULL;
	Nodo *segundo = NULL;
	Nodo *final = NULL;
	int opcion;

	cout << "Ingresa numeros a tu lista" << endl;
	while(opcion != 1){
		cout << "Ingresa un numero entero a tu lista" << endl;
		cin >> nro;
		primero = lista1->crear(nro);
		pUlt = lista1->get_ultimo();
		cout << "Para terminar de ingresar números a tu lista ingresa un 1, si no es asi, ingresa cualquier numero" << endl;
		cin >> opcion;
	}
	opcion = 0;

	cout << "Ahora ingresa los datos de la siguiente lista" << endl;
	while(opcion != 1){
		cout << "Ingresa un numero entero a tu lista" << endl;
		cin >> nro;
		segundo = lista2->crear(nro);
		cout << "Para terminar de ingresar números a tu lista ingresa un 1, si no es asi, ingresa cualquier numero" << endl;
		cin >> opcion;
	}

	mezcla(primero, segundo);

	pUlt->sgte = segundo;
	final = primero;
	cout << "Lista final:" << endl;
	final = ordenar_lista(final);
	imprimir(final);

	return 0;
}